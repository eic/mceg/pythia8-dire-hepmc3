// This is an example how to use the pythia8+HEPMC3+DIRE
// C 2020 Andrii verbytskyi andrii.verbytskyi@mpp.mpg.de

// main36.cc is a part of the PYTHIA event generator.
// Copyright (C) 2018 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Basic setup for Deeply Inelastic Scattering at HERA.




#include <cassert>
#include <sstream>
#include "Pythia8/Pythia.h"
#ifdef USEDIRE
#include "Dire/Dire.h"
#endif
#include "Pythia8Plugins/ProgressLog.h"
#include "HepMC3/WriterAscii.h"
#include "Pythia8ToHepMC3.h"
#include "optionsDIS.h"

using namespace Pythia8;
using namespace HepMC3;
int main(int argc, char** argv)
{

    gengetopt_args_info ai;
    if (cmdline_parser (argc, argv, &ai) != 0)
    {
        exit(1);
    }

    Pythia8::Pythia pythia;
    Event& event = pythia.event;
    Pythia8ToHepMC3 pythiaToHepMC;
    //Life is not easy. Pythia8 generates a broken event tree for DIS. Pythia6 had pypars.mstp[128-1].
    // Another possible solution is to manipulate the event record manually.
    pythiaToHepMC.set_crash_on_problem(false);

    pythia.settings.addFlag("Main:runRivet",false);
    pythia.settings.addFlag("Main:useDIRE",false);
    pythia.settings.addWVec("Main:analyses",vector<string>());
    pythia.settings.addParm ("Main:DISQ2MIN",5.0,true,true,0.0,100000.0);
    pythia.settings.addParm ("Main:DISQ2MAX",1000.0,true,true,0.0,100000.0);
    pythia.settings.addParm ("Main:DISYMIN",0.01,true,true,0.0,1.0);
    pythia.settings.addParm ("Main:DISYMAX",0.95,true,true,0.0,1.0);
    pythia.settings.addParm ("Main:numberOfEventsOut",1000,true,true,0,1000000);

    pythia.readFile(ai.config_arg);
    const bool useDIRE=pythia.flag("Main:useDIRE");

#ifdef USEDIRE
    Dire* dire=NULL;
    if (useDIRE)
    {
        dire= new Dire();
        dire->init(pythia);
    }
#endif
    pythia.init();
    HepMC3::Writer* ascii_io=nullptr;
    if (ai.output_hepmc_file_given>0)
    {
// New features: metainformation
        std::shared_ptr<GenRunInfo> run = std::make_shared<GenRunInfo>();
        struct GenRunInfo::ToolInfo generator= {std::string("Pythia8"),std::to_string(PYTHIA_VERSION).substr(0,5),std::string("Used generator")};
        run->tools().push_back(generator);
        struct GenRunInfo::ToolInfo config= {std::string(ai.config_arg),"1.0",std::string("Used control cards")};
        run->tools().push_back(config);
#ifdef USEDIRE
        struct GenRunInfo::ToolInfo shower_generator= {std::string("DIRE"),DIRE_VERSION,std::string("Used shower generator")};
        run->tools().push_back(shower_generator);
#endif
        /* Tool can mean something else as well... */
        char hn[128];
        gethostname(hn,128);
        struct GenRunInfo::ToolInfo host= {std::string("hostname"),std::string(hn),std::string("Used host")};
        run->tools().push_back(host);

        std::vector<std::string> names;
        for (int iWeight=0; iWeight < pythia.info.nWeights(); ++iWeight) {
            std::string s=pythia.info.weightLabel(iWeight);
            if (!s.length()) s=std::to_string((long long int)iWeight);
            names.push_back(s);
        }
        if (!names.size()) names.push_back("default");
        run->set_weight_names(names);
        ascii_io= new HepMC3::WriterAscii(ai.output_hepmc_file_arg,run);  //Note you can put any other s Writer here!
    }
    // For the following analysis we need more statistics.
    const std::vector<std::string> rAnalyses=pythia.settings.wvec("Main:analyses");
    const double DISQ2MIN=pythia.settings.parm("Main:DISQ2MIN");
    const double DISQ2MAX=pythia.settings.parm("Main:DISQ2MAX");
    const double DISYMIN=pythia.settings.parm("Main:DISYMIN");
    const double DISYMAX=pythia.settings.parm("Main:DISYMAX");
    int nEvent = pythia.mode("Main:numberOfEvents");
    if (ai.number_generate_given>0) nEvent=ai.number_generate_arg;
    // Begin event loop.
    for (int iEvent = 0; iEvent < nEvent; ++iEvent)
    {
        if (!pythia.next()) continue;
        double wt =1;
#ifdef USEDIRE
        if (dire)
        {
            dire->weightsPtr->calcWeight(0.);
            dire->weightsPtr->reset();
            wt = dire->weightsPtr->getShowerWeight();
        }
#endif
        if (std::abs(wt)<0.000000001) continue;
        //Here one should handle the wt. But to do that one should read DIRE documentation.

        // Four-momenta of proton, electron, virtual photon/Z^0/W^+-.
        Vec4 pProton = event[1].p();
        Vec4 peIn    = event[4].p();
        Vec4 peOut   = event[6].p();
        Vec4 pPhoton = peIn - peOut;

        event[0].mother1(1);  //p
        event[0].mother2(2);  //e

        // Q2, W2, Bjorken x, y.   Custom definition!!!!
        double Q2    = - pPhoton.m2Calc();
        double W2    = (pProton + pPhoton).m2Calc();
        double x     = Q2 / (2. * pProton * pPhoton);
        double y     = (pProton * pPhoton) / (pProton * peIn);

        if (Q2<DISQ2MIN)    continue;
        if (Q2>DISQ2MAX) continue;

        if (y<DISYMIN)  continue;
        if (y>DISYMAX)  continue;

        if (ascii_io&&iEvent<ai.write_to_output_arg)
        {
            HepMC3::GenEvent myevent( HepMC3::Units::GEV, HepMC3::Units::MM );
            pythiaToHepMC.fill_next_event(pythia.event, &myevent, -1, &pythia.info);
            std::shared_ptr<HepMC3::Attribute> my_q2=std::make_shared<HepMC3::DoubleAttribute>(Q2);
            myevent.add_attribute("Q2",my_q2);
            myevent.add_attribute("W2",std::make_shared<HepMC3::DoubleAttribute>(W2));//shortcut
            myevent.add_attribute("y",std::make_shared<HepMC3::DoubleAttribute>(y));//shortcut
            ascii_io->write_event(myevent);
        }
        // End of event loop. Statistics and histograms.
    }
    pythia.stat();
    if (ascii_io) delete ascii_io;
    // Done.
    return 0;
}
