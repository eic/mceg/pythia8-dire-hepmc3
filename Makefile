all:  pythia8DISDIREHEPMC3

pythia8DISDIREHEPMC3: optionsDIS.c  optionsDIS.h pythia8DIS.cc
	g++ -std=c++11 -DUSEDIRE  optionsDIS.c pythia8DIS.cc  -lpythia8 -lHepMC3 -ldire  -I/usr/share/HepMC3/interfaces/pythia8/include/Pythia8   -I/usr/include -I/usr/include -I/usr/include/Pythia8  -o pythia8DISDIREHEPMC3
	
clean:
	rm pythia8DISDIREHEPMC3

beauty: 
	astyle *.c *.h *.cc
	
	
test: pythia8DISDIREHEPMC3
	./pythia8DISDIREHEPMC3 -c pythia8DIS.dat -H test.hepmc3  -W 100